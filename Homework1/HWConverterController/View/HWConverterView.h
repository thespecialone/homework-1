//
//  HWConverterView.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 29/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWConverterView : UIView

@property (nonatomic, strong, readonly) UITextField *inputTextField;
@property (nonatomic, strong, readonly) UIButton *convertButton;
@property (nonatomic, strong, readonly) UILabel *resultLabel;

@property (nonatomic, assign) UIEdgeInsets contentInsets;

@end
